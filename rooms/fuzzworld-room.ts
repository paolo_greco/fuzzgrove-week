import { Fuzzworld } from "../src/Fuzzworld";
import { Room } from "colyseus";
import { Teleporter } from "../src/Teleporter";
import { Mob } from "../src/Mob";
import { OverlandGenerator } from "../src/overland-generator";


export class StateHandlerRoom extends Room<Fuzzworld> {
    private teleporter: Teleporter;
    public id:string;
    onInit (options) {
        this.id = options.x+':'+options.y;
        console.log("StateHandlerRoom created!", options);

        this.autoDispose = false;
        let generator = new OverlandGenerator([' ',' ','T','T','T','T','T','T','o']);
        let grid = generator.generateFuzzarea(this.id, 20);
        let world = new Fuzzworld(grid);

        this.setState(world);
        this.teleporter = new Teleporter();
        this.setSimulationInterval(
            () => { world.stepSimulation(); },
            Math.floor(1000.0 / this.state.tps)

        ); 
    }
    onJoin (client) {
        let m:Mob;
        while (m = this.teleporter.receiveMob(this.id)){
            this.state.mobs[m.id] = m;
        }

        if (!this.state.mobs[client.sessionId]){
            this.state.createPlayer(client.sessionId);
        }
    }

    onLeave (client) {
        // let m:Mob = this.state.mobs[client.sessionId];
        // if (m. ){
        //     this.teleporter.sendMob(m.destinationId,m);
        // }
        console.log("maybe it tried to move to another room?");
        this.state.removePlayer(client.sessionId);
    }

    onMessage (client, data) {
        console.log("StateHandlerRoom received message from", client.sessionId, ":", data);
        this.state.commandMob(client.sessionId, data);
    }

    onDispose () {
        console.log("Dispose StateHandlerRoom");
    }


}