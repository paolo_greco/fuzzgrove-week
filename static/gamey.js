let viewport_offset_x = 0;
let viewport_offset_y = 0;
let sprite_scaling = 4;    

var tiles = [];

initgfx();
function initgfx() {
  loadJSON('/assets/tiles.json',function(response) {      
    tiles = JSON.parse(response);
  });
}


class Animation {
    constructor(characterImages, tileSetId = 'elf_f_idle_anim'){
        this.tileSetId = tileSetId;
        this.renderCount = 0;
        this.render_per_frame = 2;
    }
    render(x,y){
        let t = tile(this.tileSetId);
        let animation_offset = 0; // separate frames have an x offset
        if (t.animLen > 1){
            let frame_num = this.renderCount >> this.render_per_frame;
            animation_offset = frame_num % t.animLen;  
            this.renderCount++;
            if (frame_num >= t.animLen){
                this.renderCount = 0;
            }
        }

        canvas.drawImage(
          characterImages ,
          t.x + animation_offset*t.w, t.y,
          t.w, t.h,
          viewport_offset_x + x - (t.w/2*sprite_scaling), viewport_offset_y + y-((t.h-4)*sprite_scaling),
          t.w*sprite_scaling, t.h*sprite_scaling
        );
    }
}


  function tile(name){        
    return tiles[name] || {
      "x": 0,
      "y": 0,
      "w": 16,
      "h": 16,
      "animLen": 1,
      "isAnim": false
    };

  }




  function loadJSON(url,callback) {   

    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open('GET', url, true); // Replace 'my_data' with the path to your file
    xobj.onreadystatechange = function () {
      if (xobj.readyState == 4 && xobj.status == "200") {
        // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
        callback(xobj.responseText);
        return;
      }
      console.log(xobj);
    };

    xobj.send(null);  
  }

  window.addEventListener("keydown", function (e) {
    if (e.which === 38) {
      up();

    } else if (e.which === 39) {
      right();

    } else if (e.which === 40) {
      down();

    } else if (e.which === 37) {
      left();
    }
  });

  function up () {
    room.send({ y: -1 });
  }

  function right () {
    room.send({ x: 1 });
  }

  function down () {
    room.send({ y: 1 })
  }

  function left () {
    room.send({ x: -1 })
  }

  function get_stick(stick_id){
    let x_axis = stick_id*2;
    var gamepads = navigator.getGamepads ? navigator.getGamepads() : (navigator.webkitGetGamepads ? navigator.webkitGetGamepads : []);
    if (gamepads[0] && gamepads[0].axes ){
      let xg = gamepads[0].axes[x_axis];
      let yg = gamepads[0].axes[x_axis+1];
      if (dist2(xg,yg)<0.01){ return null }
      return [xg,yg];
    }        
    return null;
  }
  function dist2(x,y){
    return x*x + y*y;
  }