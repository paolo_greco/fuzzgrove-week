import { MovingCollidable, collide } from "./FuzzInterfaces";
import { Mob } from "./Mob";

export class Bullet implements MovingCollidable {
    id: string;
    x: number;
    y: number;
    vx: number;
    vy: number;
    id_master:string;
    is_live: boolean;
    my_expiry_time:number;
    damage:number;
    piercing:boolean;
    
    constructor(id:string, id_master:string, x: number, y:number, vx:number, vy:number, expiry_time:number, damage: number = 3, piercing = false){
        this.id=id;
        this.id_master = id_master;
        this.x = x;
        this.y = y;
        this.vx = vx;
        this.vy = vy;
        this.is_live = true;
        this.my_expiry_time = expiry_time;
        this.damage = damage;
        this.piercing = piercing;
    }
    radius(): number {
        return 1;
    }
    public collide(p2:MovingCollidable, move_x:number, move_y:number): boolean {
        if (!this.live()){return false;}
        if (this.id_master == p2.id){ return false; }
        return collide(this,p2,move_x, move_y);
    }
    live(): boolean {
        return this.is_live;
    }
    kill(): void {
        this.is_live = false;
    }
    expiry_time(): number {
        return this.my_expiry_time;
    }    
    hit(p1: Mob): any {
        if (this.live()){
            p1.wound(this.damage);
            if (!this.piercing){this.kill()}
        }
    }
}
