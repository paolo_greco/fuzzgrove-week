 /**
 * generate a random integer between min and max
 * @param {Number} min 
 * @param {Number} max
 * @return {Number} random generated integer 
 */
export function randomInt(min, max){
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function d(n:number){ return randomInt(1,n);}
export function d0(n:number){ return randomInt(0,n-1);}
