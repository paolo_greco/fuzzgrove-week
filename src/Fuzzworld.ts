import { EntityMap, nosync } from "colyseus";
import { MovingCollidable, normalize, d, collide, normalize_if_overunity, ID } from "./FuzzInterfaces";
import { Mob } from "./Mob";
import { Bullet } from "./Bullet";



const tile_width = 64;
const move_scaling = 3;
const bullet_speedup = 2;

//const widest_collidable_radius = tile_width*2; 
const CIRCLE_QUADRATURE_RATIO = 0.8;

export class Fuzzworld {

    mobs: EntityMap<Mob> = {};
    bullets: EntityMap<Bullet> = {};  
    obstacles: EntityMap<Obstacle> = {};
    @nosync tps = 50;  

    constructor(private grid){}

    @nosync commands_received: Map<String,any> = new Map();
    current_time: number = 0;

    createPlayer (id: string):Mob {
        return (this.mobs[ id ] = new Mob(
            id,
            Math.floor(Math.random() * 300),
            Math.floor(Math.random() * 300),
            0,
            this
        ));
    }
    createMob(id: string):Mob{
        let p = this.createPlayer(id);
        p.setAI(new AI());
        return p;
    }
    createObstacle(x,y){
        let id = 'o'+ID.get();
        return (this.obstacles[ id ] = new Obstacle(
            id,
            x*tile_width,
            y*tile_width
        ));
    }
    playerAvatars() : Array<Mob> {
        return Object.keys(this.mobs)
            .map( key => { return this.mobs[key]; })
            .filter(p=>{
                if (p.team()=='ai'){ return false; }
                return true;
            });
    }
    removePlayer (id: string) {
        return this.removeMob(id);
    }
    removeMob (id: string) {
        delete this.mobs[ id ];
    }
    addMob(m:Mob){
        this.mobs[ m.id ] = m;
    }
    addBullet(b: Bullet){
        this.bullets[b.id] = b;
    }
    removeBullet( b: Bullet){
        delete this.bullets[ b.id ];
        b.kill();
        //better would to stash & recycle them
    }
    removeBulletById( id:string){
        let b = this.bullets[id];
        if (b){this.removeBullet(b)}
    }

    commandMob (id: string, command: any) {
        this.commands_received.set(id,command);
    }
    now() : number{
        return this.current_time;
    }
    stepSimulation(){
        this.current_time++;
        // this.commands_received.forEach((value: any, key: string) => {
        //     console.log(key, value);
        // });

        let myObstacles =
            Object
                .keys(this.obstacles)
                .map( key => { return this.obstacles[key]; });

        let myPlayers = Object.keys(this.mobs)
            .map( key => { return this.mobs[key]; })
            .filter(p=>{
                if (p.live()){ return true; }
                this.removeMob(p.id);
            });
        let myBullets = Object.keys(this.bullets)
            .map( key => { return this.bullets[key]; })
            .filter( (b) => {
                let gap = 200;
                if (
                    //(b.x < -gap || b.x > this.width+gap || b.y < -gap || b.y > this.height+gap ) ||
                    !b.live() ||
                    b.expiry_time() < this.current_time 
                    ){
                    this.removeBullet(b);
                    return false;
                }
                b.x += b.vx*bullet_speedup*move_scaling;
                b.y += b.vy*bullet_speedup*move_scaling;
                return true;
        });

        myPlayers.forEach((p1)=>{
            //first, see if a bullet hits the player.

            myBullets.forEach((b) => {
                if (b.live() && b.collide(p1,0,0)){
                    b.hit(p1);
                }                 
            });

            // if the ID is in the AIs, get the AI command
            // else, get a command from the commands
            // else, nothing! 

            let command =
                this.commands_received.get(p1.id) ||
                p1.ai(this) ||
                { };

            if (command.shoot && p1.can('shoot') && (command.shoot.x || command.shoot.y)){
                let v = normalize(command.shoot.x||0, command.shoot.y||0);
                let shot = p1.shoot(v[0],v[1], this.tps << 2);
                this.addBullet(shot);
            }

            if (command.pickup && p1.can('pickup')){
                
            }
            
            //stop player if it hits another player
            // we should cap the movement to 1 
                
            //world margin            
            // if (p1.x - p1.size() < 0) { p1.x = p1.size(); }
            // else if (p1.x+p1.size() > this.width) { p1.x = this.width-p1.size()}
            // if (p1.y - p1.size()< 0) {p1.y = p1.size()}
            // else if (p1.y+p1.size() > this.height) { p1.y = this.height-p1.size()}
            
            if (command.x || command.y){
                
                let movement = normalize_if_overunity( command.x, command.y );

                let dx = (movement[0]*move_scaling) || 0;
                let dy = (movement[1]*move_scaling) || 0;

                myPlayers.some((p2)=>{
                    if (!dx && !dy){return;}
                    if (p1 != p2 && p1.collide(p2, dx, dy)){
                        //fermo!
                        dx = 0; dy = 0;
                        return true;
                    }
                });

                if (dx || dy){
                    myObstacles.some((o)=>{
                        if (o.collide(p1, dx, dy)){
                            //fermo!
                            dx = 0; dy = 0;
                            return true;
                        }
                    });

                    if (this.myGridCollidesWith(p1, dx, dy)){
                        dx = 0; dy = 0;
                    }
                }
            
                //se nessuno lo ha fermato, muovi!
                if (dx || dy){
                    p1.moveBy(dx, dy);
                }

            }

        });
        this.commands_received.clear();
    } 
    myGridCollidesWith(o: MovingCollidable, dx:number, dy:number): boolean {
        let range : number = o.radius() * CIRCLE_QUADRATURE_RATIO;

        let min_x = Math.max(Math.floor(o.x - range), 0);
        let max_x = Math.min(Math.floor(o.x + range), this.grid.length);
        let min_y = Math.max(Math.floor(o.y - range), 0);
        let max_y = Math.min(Math.floor(o.y + range), this.grid.length);

        var x:number, y:number;

        for (x = min_x; x <= max_x ; x++){
            for (y = min_y; y <= max_y ; y++){
                let cell_content = this.grid[x][y];
                if ( cell_content && this.grid[x][y] != ' '){
                    //ok, we hit something
                    // however we should try to zero either dx or dy and see if the mob can strafe
                    return true;
                }
            }
        }
        return false;
     }
}



// class Item implements MovingCollidable {
//     x: number;
//     y: number;
//     id: string;
//     radius(): number {
//         return 10;
//     }
//     collide(c: MovingCollidable, move_x: number, move_y: number): boolean {
//         return collide(this,c,move_x, move_y);
//     }
//     constructor(id:string, x: number, y:number){
//         this.id=id;
//         this.x = x;
//         this.y = y;
//     }
// }


class Obstacle {
    x: number;
    y: number;
    id: string;
    size(): number {
        return 32;
    }
    collide(c: MovingCollidable, move_x: number, move_y: number): boolean {
        let dx = Math.abs(c.x - this.x + move_x);
        let dy = Math.abs(c.y - this.y + move_y);
        let bulk = c.radius() * CIRCLE_QUADRATURE_RATIO; // make it into a smaller square
        let minimum_gap = bulk + this.size();
        return  (
            dx < minimum_gap &&
            dy < minimum_gap
        )
    }
    constructor(id:string, x: number, y:number){
        this.id=id;
        this.x = x;
        this.y = y;
    }
}



export class AI {
    dx:number;
    dy:number;
    constructor(){
        this.dx = 0;
        this.dy = 0;
    }
    command(p: Mob, state: Fuzzworld): any {
        let command = {};

        //random move and shoot at player
        if (p.cooldown('AI-move',d(50)+30)){
            let v = normalize( d(11)-6, d(11)-6 );
            this.dx = v[0];
            this.dy = v[1]; 
        }
        command['x'] = this.dx;
        command['y'] = this.dy;

        // if (p.cooldown('AI-shoot')){
        //     let players = state.playerAvatars();
        //     if (players.length){
        //         let target = players[d(players.length)-1];
        //         command['shoot'] = {
        //             x: target.x-p.x, y: target.y-p.y
        //         };
                
        //     } 
        // }
        
        return command;
    }

}

