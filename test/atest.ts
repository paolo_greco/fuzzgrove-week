import {OverlandGenerator} from '../src/overland-generator';
import {expect} from 'chai';
import {} from 'mocha';


describe('model', () => {
    it('should return barren when unconfigured', () => {
        let og = new OverlandGenerator();

        expect(og.generatemap(3,3)).to.eql(
            [
                ['.','.','.'],
                ['.','.','.'],
                ['.','.','.']
                
            ]
        );
    });
    it('should return mountaints when configured with mountains', () => {
        let og = new OverlandGenerator(['^','.']);
        expect(og.generatemap(2,2)).to.eql(
            [
                ['^','^'],
                ['^','^'],
                
            ]
        );
    })
});