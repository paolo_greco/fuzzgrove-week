import { Server } from 'colyseus';
import { Mob } from './Mob';

export class Teleporter {
    private static myServer:Server = null;
    private static map : Map<string,Mob[]> = new Map<string,Mob[]>();
    constructor(){
    }
    register(gameServer:Server){
        Teleporter.myServer = gameServer;
    }
    sendMob(destination_id:string, m:Mob):void{
        let content = Teleporter.map.get(destination_id);
        if (!content){
            content = [];
            Teleporter.map.set(destination_id,content);
        }
        content.push(m);
    }
    receiveMob(destination_id:string):Mob{
        let content = Teleporter.map.get(destination_id);
        if (!content){
            return null;
        }
        return content.shift();
    }



}