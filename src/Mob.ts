
import { EntityMap, nosync } from "colyseus";
import { AI, Fuzzworld } from "./Fuzzworld";
import { MovingCollidable, collide, ID } from "./FuzzInterfaces";
import { Bullet } from "./Bullet";

export class Mob implements MovingCollidable{
    id: string
    x: number;
    y: number;
    hp : number; hp_max:number;
    lookslike: number;
    @nosync room: Fuzzworld;
    cooldowns: EntityMap<number> = {};
    myAI:AI = null;
    constructor(id:string, x:number, y:number, lookslike:number, state:Fuzzworld, hp_max : number = 10){
        this.id = id;
        this.x = x;
        this.y = y;
        this.lookslike = lookslike;
        this.room = state;
        this.hp = this.hp_max = hp_max;
    }
    public setAI(ai:AI):void{
        this.myAI = ai;
    }
    public team(): string{
        return (this.myAI?'ai':'human');
    }
    public collide(p2:MovingCollidable, move_x:number, move_y:number): boolean {
        return collide(this,p2,move_x, move_y);
    }
    public moveBy(dx:number, dy:number): void {
        this.x += dx;
        this.y += dy;
    }
    public ai(state:Fuzzworld): any{
        if (this.myAI){return this.myAI.command(this,state);}
        return null;

    }
    public shoot(vx:number, vy:number, ttl:number):Bullet{
        this.cooldown('shoot');
        return new Bullet(
            'b'+ID.get(),
            this.id,
            this.x,
            this.y,
            vx,vy,
            this.room.now()+ttl );
    }
    cooldown(name:string, delay:number = this.room.tps):number{
        let current = this.cooldowns[name] || 0;
        if (this.room.now() >= current){
            return (this.cooldowns[name] = this.room.now()+delay);
        }
        return 0;
    }
    can(command: string): boolean {
        let now:number = this.room.now();
        return (this.cooldowns[command] || 0) <= now ;
    }
    wound(damage:number):void{
        if (this.cooldown('hurt',this.room.tps)){
            this.hp -= damage;
        }
    }
    live(): any {
        return this.hp > 0;
    }
    
    public radius(): number { return 16; }
}