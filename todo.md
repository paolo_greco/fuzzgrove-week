## room transfer
1. send message with room destination. change room id in mob if necessary
1. disconnect client. onLeave -> save mob in the letterbox
1. client joins new room
1. onJoin get stuff from letterbox. if nothing, kick out 


# needed stuff
1. obstacles in a grid
1. add content to rooms
1. add demon crypts
1. add endgame with tombstones and characters next to piles of skulls
1. add buffs


## cool stuff
+ boid behaviour. set high gap for harder-to-hit mobs
+ dafsfasd
+ to implement seamless overland, each room can look at margins of adjoining rooms. no need to pump updates as it's all synchronous
+ ignore obstacles in current and first square of trajectory to make possible firing under cover (actually check from distance two)
+ 2 seconds cooldown before leaving a room