import {d, d0} from "./funcs";
let seedrandom = require('seedrandom');

export class OverlandGenerator {
    
    constructor(private terrains : string[] = [' ']){
        // let easystarjs = require('easystarjs');
        // this.easystar = new easystarjs.js();
    }

    generatemap(seed: string, width: number, rows: number): GridMap {
        let gridmap = new GridMap(seed,width,rows);
        let terrains_number = this.terrains.length;
        let map = [];

        for (let r=0; r<rows; r++){
            let row = [];
            for (let i=0; i<width; i++ ){
                row.push(this.terrains[gridmap.d0(terrains_number)]);
            }
            map.push(row);

        }
        gridmap.grid = map;
        return gridmap;
        
    }
    generateFuzzarea(name:string, size:number = 6) {
        let map = this.generatemap(name, size,size);
        let w2 = size>>1;
        let r2 = size>>1;
        //let's walk
        let walkers = [
            new DrunkWalker('n',0,w2, map),
            new DrunkWalker('e', r2, 0, map),
            new DrunkWalker('s',size-1,w2, map),
            new DrunkWalker('w',r2,size-1, map)
        ];
        do {
            walkers.forEach( w => w.wander() );
         } while( walkers.filter(w=>w.isDone()).length < 4 );
        return map.grid;
    }

}


class GridMap {
    grid : string[][];
    rng : Function = null;
    piss : Map<String,String>;
    constructor( public seed: string, public width: number, public rows:number ){
        this.rng = seedrandom(seed);
        this.piss = new Map();
    }
    d0(n:number){ return Math.floor(this.rng() * n); }
    contains(x:number,y:number){
        return (x>=0 && y >=0 && x<this.width && y<this.rows);
    }
    neighbourCoords(x,y){
        return [[0,1],[1,0],[-1,0],[0,-1]]
            .map( ([dx,dy]) => { return [ x+dx, y+dy ] } )
            .filter( ([x,y]) => { return this.contains(x,y)} );
    }
    dump(){
        console.log(this.seed);
        this.grid.forEach(row => {
            console.log(row.join(''));
        });
    }
}

class DrunkWalker {
    private done:boolean;
    constructor(private name:string, private x:number, private y:number, private map:GridMap){
        this.done = false;
    }
    isDone(){ return this.done; }
    d0(n:number){ return this.map.d0(n); }

    wander(){
        if (this.isDone()){ return; }
        this.map.grid[this.x][this.y] = '.';
        if (this.canSmellPissAround()){
            this.done = true;
            return;
        }
        //piss
        this.map.piss.set(this.x+':'+this.y, this.name);
        //wander
        let neighbours = this.map.neighbourCoords(this.x, this.y);
        let [x_next, y_next]  = neighbours[this.d0(neighbours.length)];
        this.x = x_next;
        this.y = y_next;
        return;
    }
    canSmellPissAround(): any {
        return this.map.
            neighbourCoords(this.x,this.y).
            some(([x,y])=>{ 
                let smell = this.map.piss.get(x+":"+y);
                return (smell && (smell != this.name));
                });

    }
    
}

// let og = new OverlandGenerator(['T','T','T',' ','~','.']);
// og.generateFuzzarea('foo',40,40);
// og.generateFuzzarea('bar',40,40);