export function collide (c1: MovingCollidable, c2: MovingCollidable, d_x_1, d_y_1: number){
    let dx = (c1.x - c2.x + d_x_1);
    let dy = (c1.y - c2.y + d_y_1);
    let distance_2 = dx*dx + dy*dy;

    let min = c1.radius() + c2.radius();

    return (distance_2 <= (min*min));
}

export function normalize_if_overunity (x:number, y:number){
    let length2 = (x*x + y*y);
    if (length2 <= 1){
        return [x,y];
    }

    var l = 1.0 / ( Math.sqrt(length2) || 1 );
    x *= l;
    y *= l;
    return [x,y];
};
export function normalize (x:number, y:number){
    var l = 1.0 / ( Math.sqrt(x*x + y*y)|| 1 );
    x *= l;
    y *= l;
    return [x,y];
};

export interface MovingCollidable {
    x: number;
    y: number;
    readonly id: string;
    radius(): number;
    collide(c:MovingCollidable, move_x:number, move_y:number):boolean;
}

export function d(n:number):number{
    return Math.floor(Math.random()*n)+1;
}

export class ID {
    static lastId:number = 0;
    
    static get(prefix:string = ''):string {
        return prefix+(++ID.lastId);
    }
}